package leetcode;
/*
Given an integer x, return true if x is palindrome integer.

An integer is a palindrome when it reads the same backward as forward. For example, 121 is palindrome while 123 is not.
 */

public class PalindromeNumber {
    public static boolean isPalindrome(int x) {
        if (x < 0) {
            return false;
        }

        int y = 0;
        int z = x;
        while (z != 0) {
            y = y * 10 + z % 10;
            z = z / 10;
        }
        return x == y;
    }

    public static void main(String[] args) {
        System.out.println(isPalindrome(121));
    }
}
