package leetcode;
/*
Given a signed 32-bit integer x, return x with its digits reversed.
If reversing x causes the value to go outside the signed 32-bit integer range [-231, 231 - 1], then return 0.
 */

public class ReverseInteger {
    static long result = 0;
    static int power = 1;

    public static int reverse(int x) {
        if (x == 0) {
            return x;
        }
        reverse(x / 10);
        result += (long) x % 10 * power;
        if (result > Integer.MAX_VALUE || result < Integer.MIN_VALUE) {
            return 0;
        }
        power *= 10;
        return (int) result;
    }

    public static void main(String[] args) {
        System.out.println(reverse(1534236469));
    }
}
