package leetcode;
/*
Write a function to find the longest common prefix string amongst an array of strings.

If there is no common prefix, return an empty string "".
 */
public class LongestCommonPrefix {
    public static String longestCommonPrefix(String[] strs) {
        StringBuilder result = new StringBuilder();
        if (strs.length > 200 || strs.length < 1 || strs[0].length() == 0) {return result.toString();}
        if (strs.length == 1) {return strs[0];}

        int n = 0;

        while(true) {
            int count = 0;
            for (int i = 1; i < strs.length; i++) {
                if (strs[i].length() == n || strs[i].length() >= 200 || strs[i].length() == 0 || strs[0].length() == n) {
                    return result.toString();
                }
                if (strs[i].charAt(n) == strs[0].charAt(n)) {
                    count++;
                } else {
                    return result.toString();
                }
            }
            if (count == strs.length - 1) {
                result.append(strs[0].charAt(n));
            }
            n++;
        }
    }

    public static void main(String[] args) {
        System.out.println(longestCommonPrefix(new String[]{"ac","ac","ac"}));
        System.out.println(longestCommonPrefix(new String[]{"a","ac","ac"}));
        System.out.println(longestCommonPrefix(new String[]{"flower","flow","flight"}));
    }
}
