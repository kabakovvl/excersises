public class Swap {
    public static void main(String[] args) {
        Integer a = 1;
        Integer b = 2;
        System.out.println(a + "," + b);
        swap(a, b);
    }

    public static void swap(Integer a, Integer b) {
        a = a + b;
        b = a - b;
        a = a - b;
        System.out.println(a + "," + b);
    }
}
