import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

//Работа с итераторами

public class Iterators {
    public static void main(String[] args) {
        List<Integer> testList = new LinkedList<>();
        testList.add(1);
        testList.add(2);
        testList.add(3);
        testList.add(4);
        testList.add(5);
        testList.add(6);

        System.out.println(testList);
        int i = 0;
        ListIterator<Integer> iter = testList.listIterator();
        while (iter.hasNext()) {
            i++;

            iter.next();
            if (i == 4) {
                iter.remove();
                iter.add(44);
            }
        }

        System.out.println(testList);

//        ListIterator позволяет начать с любого индекса
        iter = testList.listIterator(5);
        iter.next();
        iter.remove();

        System.out.println(testList);
    }
}
