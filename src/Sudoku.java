import java.util.Arrays;
import java.util.LinkedList;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/*
Determine if a 9x9 Sudoku board is valid. Only the filled cells need to be validated according to the following rules:

1) Each row must contain the digits 1-9 without repetition.
2) Each column must contain the digits 1-9 without repetition.
3) Each of the 9 3x3 sub-boxes of the grid must contain the digits 1-9 without repetition.

The Sudoku board could be partially filled, where empty cells are filled with the character '.'
*/
public class Sudoku {

    private static boolean isValidSudoku(char[][] board) {
//        Case 1

        for (char[] boardRow : board) {
            if (!checkRow(boardRow)) {
                System.out.println("A row contains repetition!");
                return false;
            }
        }
        System.out.println("Rows: OK");

//        Case 2

//        Transpose matrix
        for (int i = 0; i < board.length; i++) {
            for (int j = i; j < board.length; j++) {
                char tmp = board[i][j];
                board[i][j] = board[j][i];
                board[j][i] = tmp;
            }
        }

        for (char[] boardRow : board) {
            if (!checkRow(boardRow)) {
                System.out.println("A column contains repetition!");
                return false;
            }
        }
        System.out.println("Columns: OK");

//        Case 3

        final int SQUARE_SIZE = 3;

//        Convert board matrix to String
        String strBoard = Arrays.stream(board).map(String::valueOf).collect(Collectors.joining());

//        Divide board onto 3x3 squares
        char[][] square = new char[SQUARE_SIZE][SQUARE_SIZE];

//        Position of 3x3 matrix element in string view
        int k = 0;
//        Shear ratio for next block of 3x3 matrices
        int m = 0;
        for (int n = 0; n < board.length * SQUARE_SIZE; n = n + SQUARE_SIZE) {
            if (n % board.length == 0 && n != 0) {
                m++;
            }
            for (int i = 0; i < SQUARE_SIZE; i++) {
                k = n + (i + m * 2) * board.length;
                for (int j = 0; j < SQUARE_SIZE; j++) {
                    square[i][j] = strBoard.charAt(k);
                    k++;
                }
            }

//            Convert matrix to array
            char[] boardRow = Arrays.stream(square)
                    .map(String::valueOf)
                    .collect(Collectors.joining())
                    .toCharArray();

            if (!checkRow(boardRow)) {
                System.out.println("A 3x3 matrix contains repetition!");
                return false;
            } else {
                System.out.println("3x3 matrices: OK");
            }
            System.out.println(Arrays.deepToString(square));
        }

        return true;
    }

    private static boolean checkRow(char[] boardRow) {
        Stream<Character> chStream = IntStream.range(0, boardRow.length).mapToObj(n -> boardRow[n]);
        LinkedList<Character> list = chStream.filter(ch -> ch > 48 && ch < 58)
                .collect(Collectors.toCollection(LinkedList::new));
        while (!list.isEmpty()) {
            char selected = list.poll();
            if (list.contains(selected)) {
                System.out.println(Arrays.toString(boardRow));
                return false;
            }
        }
        return true;
    }


    public static void main(String[] args) {

        char[][] board = new char[][]{
                {'5', '3', '.', '.', '7', '.', '.', '.', '.'},
                {'6', '.', '.', '1', '9', '5', '.', '.', '.'},
                {'.', '9', '8', '.', '.', '.', '.', '6', '.'},
                {'8', '.', '.', '.', '6', '.', '.', '.', '3'},
                {'4', '.', '.', '.', '.', '3', '.', '.', '1'},
                {'7', '.', '.', '.', '2', '.', '.', '.', '6'},
                {'.', '.', '.', '.', '.', '.', '2', '8', '.'},
                {'.', '6', '.', '4', '1', '9', '.', '.', '5'},
                {'.', '.', '.', '.', '8', '.', '.', '7', '9'}
        };

        boolean res = isValidSudoku(board);

        System.out.println(res);
    }
}