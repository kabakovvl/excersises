import java.util.HashMap;
import java.util.Map;

public class Nulls {

    /**
     * An example of Autoboxing and NullPointerException
     *
     * @author WINDOWS 8
     */

    public static void main(String[] args) throws InterruptedException {
        Map<Integer, Integer> numberAndCount = new HashMap<>();
        int[] numbers = {3, 5, 7, 9, 11, 13, 17, 19, 2, 3, 5, 33, 12, 5};
        for (int i : numbers) {
//            int count = numberAndCount.get(i);
//            numberAndCount.put(i, count++);
// NullPointerException here

        }

        Integer iAmNull = (Integer)null;
        if (iAmNull instanceof Integer) {
            System.out.println("iAmNull — экземпляр Integer");
        } else {
            System.out.println("iAmNull не является экземпляром Integer");
        }
    }
}
