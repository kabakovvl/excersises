public class Palindrome {
    public static void main(String[] args) {
        System.out.println(check("mama"));

        System.out.println(checkAlt("maam"));
    }

    public static boolean check(String str) {
        return str.equals(new StringBuilder(str).reverse().toString());
    }

    public static boolean checkAlt(String str) {
        char[] chars = str.toCharArray();
        for (int i = 0; i < chars.length/2; i++) {
            if (chars[i] != chars[chars.length-i-1]) {
                return false;
            }
        }
        return true;
    }
}
