import java.util.Arrays;
import java.util.LinkedList;

public class Recursion {
    public static void main(String[] args) {
        LinkedList<Long> numbers = new LinkedList<>(Arrays.asList(1L, 2L, 3L, 4L, 5L));
        System.out.println(sum(numbers));
        LinkedList<Long> numbers2 = new LinkedList<>(Arrays.asList(1L, 2L, 3L, 4L, 5L));
        System.out.println(count(numbers2));
    }

//    Find array elements sum with recursion
    static long sum(LinkedList<Long> x) {
        if (x.size() == 0) {
            return 0;
        }
        return x.poll() + sum(x);
    }

//    Count array elements with recursion
    static long count(LinkedList<Long> x) {
        if (!x.iterator().hasNext()) {
            return 0;
        }
        x.poll();
        return count(x) + 1;
    }
}
